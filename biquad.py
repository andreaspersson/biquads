import numpy as np
from ctypes import cdll, c_void_p, c_uint

lib = cdll.LoadLibrary("libbiquad.so")

def c_filtfilt(b, a, y):
    assert (len(b) == 3)
    assert (len(a) == 3)

    B = np.array(b, dtype=np.float64)
    A = np.array(a, dtype=np.float64)
    Y = np.array(y, dtype=np.float64)

    lib.filtfilt(
        c_void_p(B.ctypes.data),
        c_void_p(A.ctypes.data),
        c_void_p(Y.ctypes.data),
        c_uint(Y.size))

    return Y
