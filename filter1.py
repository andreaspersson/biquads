
import numpy as np
import pylab as pl

from scipy import signal
from biquad import c_filtfilt

t = np.linspace(0, 5e-5, 1000, False)                #  50 us, fs = 20 MHz
sig = np.sin(2*np.pi*1e5*t) + np.sin(2*np.pi*4e5*t)  #  100 kHz and 400 kHz

b, a = signal.butter(2, 2e5, 'low', fs=2e7)

filt1 = signal.filtfilt(b, a, sig)
filt2 = c_filtfilt(b, a, sig)

fig, (ax1, ax2) = pl.subplots(2, 1, sharex=True)

ax1.plot(t, sig)
ax1.set_title('100 kHz and 400 kHz sinusoids')

ax2.plot(t, filt1, label='python')
ax2.plot(t, filt2, label='c++')
ax2.set_title('After 200 kHz low-pass filter')
ax2.set_xlabel('Time [seconds]')
ax2.legend()

pl.tight_layout()
pl.show()
