#include <stddef.h>
#include <stdbool.h>
#include "biquad.h"

#define BQN 3

typedef struct {
    double  a[BQN]; /* Polynomial a coefficients */
    double  b[BQN]; /* Polynomial b coefficients */
    double  z1, z2; /* Filter state */
} biquad_t;


static void biquad_process(biquad_t *bq, double* src, double *dst, size_t n_samples, int stride)
{
    double *px = src;
    double *py = dst;

    for (size_t i = 0; i < n_samples; i++) {
        double x = *px;

        *py = bq->b[0]*x + bq->z1;
        bq->z1 = bq->b[1]*x - bq->a[1]*(*py) + bq->z2;
        bq->z2 = bq->b[2]*x - bq->a[2]*(*py);

        px += stride;
        py += stride;
    }
}


void filtfilt(double *b, double *a, double *buffer, size_t n_samples)
{
  biquad_t bq;

  for (int i=0; i<BQN; i++) {
    bq.a[i] = a[i];
    bq.b[i] = b[i];
  }

  /* Initial conditions */
  bq.z2 = buffer[0] * (b[2] - a[2]);
  bq.z1 = buffer[0] * (b[1] - a[1]) + bq.z2;

  /* Forward direction */
  biquad_process(&bq, buffer, buffer, n_samples, 1);

  /* Reversed direction */
  biquad_process(&bq, &buffer[n_samples - 1], &buffer[n_samples - 1], n_samples, -1);
}
