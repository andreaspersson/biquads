import numpy as np
import pylab as pl

from scipy import signal
from scipy.io import loadmat
from biquad import c_filtfilt

mat = loadmat("filter_implementaton_test2.mat")

t  = mat['t'][:,0]
y  = mat['yt'][:,0]
a  = mat['a'][0,:]
b  = mat['b'][0,:]

y_filt1 = mat['y_filtfilt'][:,0]

# padding
yp = np.zeros(y.size + 12)
yp[:6] = y[0]
yp[6:-6] = y
yp[-6:] = y[-1]

y_filt2 = c_filtfilt(b, a, yp)[6:-6]

print("max diff:", max(abs(y_filt1 - y_filt2)))

fig, (ax1, ax2) = pl.subplots(2, 1, sharex=True)

ax1.set_title('Filter responses')
ax1.plot(t, y, 'k', label='y: unfiltered')
ax1.plot(t, y_filt1, 'b', label='y_filt1: matlab filtfilt')
ax1.plot(t, y_filt2, 'r', label='y_filt2: c++')
ax1.legend()

ax2.set_title('Difference')
ax2.plot(t, y_filt1 - y_filt2, label="y_filt1 - y_filt2")
ax2.set_xlabel('Time [s]')
ax2.legend()
pl.tight_layout()
pl.show()
