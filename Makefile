
CFLAGS = -O2 -Wall -fPIC

all: libbiquad.so

libbiquad.so: biquad.o
	$(CC) -shared -o $@ $<

clean:
	rm -f *.o *.so
