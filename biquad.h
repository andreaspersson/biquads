#ifndef BIQUAD_H_
#define BIQUAD_H_

#ifdef __cplusplus
extern "C" {
#endif

void filtfilt(double *b, double *a, double *buffer, size_t n_samples);

#ifdef __cplusplus
}
#endif

#endif // BIQUAD_H_
